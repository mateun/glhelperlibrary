// GLHelperLib.cpp : Defines the functions for the static library.
//
#include "include/gl_helper.h"
#include "include/glm/gtc/matrix_transform.hpp"
#include "include/glm/gtc/type_ptr.hpp"
#include <SDL.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


FGLResult f_createVertexBuffer(std::vector<float> data, FVBuffer* target) {
    FGLResult result = { true, "" };

    GLuint vb;
    glGenBuffers(1, &vb);
    glBindBuffer(GL_ARRAY_BUFFER, vb);
    glBufferData(GL_ARRAY_BUFFER, data.size() * 4, data.data(), GL_STATIC_DRAW);

    GLenum err = glGetError();
    if (err != 0) {
        result.ok = false;
        result.errorMessage = glGetError();
        return result;
    }

    target->_id = vb;

    return result;
}


FGLResult f_createIndexBuffer(std::vector<uint32_t> data, FIBuffer* target) {
    FGLResult result = { true, "" };

    GLuint ib;
    glGenBuffers(1, &ib);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, data.size() * 4, data.data(), GL_STATIC_DRAW);

    GLenum err = glGetError();
    if (err != 0) {
        result.ok = false;
        result.errorMessage = glGetError();
        return result;
    }

    target->_id = ib;

    return result;
}

FGLResult f_createShader(const std::string& vertexShaderSource, const std::string& fragmentShaderSource, FShader* target) {
    FGLResult result = { true, "" };


    GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vssource_char = vertexShaderSource.c_str();
    glShaderSource(vshader, 1, &vssource_char, NULL);
    glCompileShader(vshader);
    GLint compileStatus;
    glGetShaderiv(vshader, GL_COMPILE_STATUS, &compileStatus);
    if (GL_FALSE == compileStatus) {
        result.ok = false;
        GLint logSize = 0;
        glGetShaderiv(vshader, GL_INFO_LOG_LENGTH, &logSize);
        std::vector<GLchar> errorLog(logSize);
        glGetShaderInfoLog(vshader, logSize, &logSize, &errorLog[0]);
        result.errorMessage = errorLog.data();
        glDeleteShader(vshader);
        return result;

    }


    GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fssource_char = fragmentShaderSource.c_str();
    glShaderSource(fshader, 1, &fssource_char, NULL);
    glCompileShader(fshader);

    glGetShaderiv(fshader, GL_COMPILE_STATUS, &compileStatus);
    if (GL_FALSE == compileStatus) {
        result.ok = false;
        GLint logSize = 0;
        glGetShaderiv(fshader, GL_INFO_LOG_LENGTH, &logSize);
        std::vector<GLchar> errorLog(logSize);
        glGetShaderInfoLog(fshader, logSize, &logSize, &errorLog[0]);
        result.errorMessage = errorLog.data();
        glDeleteShader(fshader);
        return result;

    }

    GLuint p = glCreateProgram();
    glAttachShader(p, vshader);
    glAttachShader(p, fshader);
    glLinkProgram(p);

    glGetProgramiv(p, GL_LINK_STATUS, &compileStatus);

    if (GL_FALSE == compileStatus) {
        GLint maxLength = 0;
        glGetProgramiv(p, GL_INFO_LOG_LENGTH, &maxLength);
        std::vector<GLchar> infoLog(maxLength);
        glGetProgramInfoLog(p, maxLength, &maxLength, &infoLog[0]);
        result.ok = false;
        result.errorMessage = infoLog.data();

        glDeleteProgram(p);
        glDeleteShader(vshader);
        glDeleteShader(fshader);

    }

    GLenum err = glGetError();
    if (err != 0) {
        result.ok = false;
        result.errorMessage = err;
        return result;
    }

    glDeleteShader(vshader);
    glDeleteShader(fshader);

    target->_shaderId = p;

    return result;

}

FGLResult f_createVAO(FVAO* target) {
    GLuint vao;
    glCreateVertexArrays(1, &vao);
    glBindVertexArray(vao);
    target->vaoId = vao;
    return { true };
}

FGLResult f_unbindVAO() {
    glBindVertexArray(0);
    return { true };
}

FGLResult f_setVertexAttribute(GLuint slot, GLenum type, int nrOfComponents) {

    glVertexAttribPointer(slot, nrOfComponents, type, GL_FALSE, 0, (void*) nullptr);
    glEnableVertexAttribArray(slot);
    return { true };


}

FGLResult f_setMatrixUniform(GLuint slot, glm::mat4 matrix) {
    glUniformMatrix4fv(slot, 1, false, glm::value_ptr(matrix));
    return { true };
}



FGLResult f_createTextureRGBA(int w, int h, void* pixelData, FTexture* target) {
    GLuint handle;
    glGenTextures(1, &handle);
    glBindTexture(GL_TEXTURE_2D, handle);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelData);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBindTexture(GL_TEXTURE_2D, 0);
    target->texId = handle;
    target->w = w;
    target->h = h;

    return { true };
}

FGLResult f_createFrameBuffer(int w, int h, GLuint* colorTexture, GLuint* depthTexture, GLuint* fbo) {
    GLuint fb;
    glGenFramebuffers(1, &fb);
    glBindFramebuffer(GL_FRAMEBUFFER, fb);

    if (colorTexture != nullptr) {
        unsigned int texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, w, h);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
        *colorTexture = texture;
    }

    if (depthTexture) {
        unsigned int texdepth;
        glGenTextures(1, &texdepth);
        glBindTexture(GL_TEXTURE_2D, texdepth);
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, w, h);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texdepth, 0);
        *depthTexture = texdepth;
    }



    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE) {
        *fbo = fb;
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return { true };
    }
    else {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return FGLResult{ false, "Framebuffer incomplete!" + glGetError() };
    }



}

std::string readFile(const std::string& file)
{
    
    std::string result = "";
    FILE* f;
    fopen_s(&f, file.c_str(), "rb");
    fseek(f, 0, SEEK_END);
    int size = ftell(f);
    rewind(f);

    char* buf = new char[size + 1];
    memset(buf, 0, sizeof(buf));
    int read = fread_s(buf, size, 1, size, f);
    if (read != size) {
        return "";
    }
    buf[size] = '\0';

    result = buf;
    fclose(f);
    return result;

}

FVAO* createDebugGridVAO(int numberOfLines) {
  
        FVAO* vao = new FVAO();
        f_createVAO(vao);

        // First we create some lines along the z axis.
        std::vector<float> pos;

        for (int i = -100; i < 100; i++) {
            pos.push_back(i);
            pos.push_back(0);
            pos.push_back(100);
            pos.push_back(i);
            pos.push_back(0);
            pos.push_back(-100);
        }

        FVBuffer vp;
        f_createVertexBuffer(pos, &vp);
        f_setVertexAttribute(0, GL_FLOAT, 3);
        vao->numberOfVertices = 200 * 2;
        f_unbindVAO();
        return vao;

}



void drawDebugGrid(FVAO* gridVAO, const FShader& shader, const Camera& camera) {
       
        glBindVertexArray(gridVAO->vaoId);
        glUseProgram(shader._shaderId);
        //glm::mat4 mat_proj = glm::perspective<float>(26, 1.3, 0.01, 500);
        glm::mat4 mat_proj = camera.projMatrix;
        glm::mat4 model = glm::translate(glm::mat4(1), glm::vec3(0, 0, 0));
        glm::mat4 view = camera.viewMatrix;

        glUniformMatrix4fv(6, 1, false, glm::value_ptr(model));
        glUniformMatrix4fv(7, 1, false, glm::value_ptr(view));
        glUniformMatrix4fv(8, 1, false, glm::value_ptr(mat_proj));
        glUniform1i(100, 1);
        glUniform4fv(101, 1, glm::value_ptr(glm::vec4(0.3, 0.4, 0.2, 1)));
        glUniform1i(102, 0);
        glUniform1i(104, 0);

        glDrawArrays(GL_LINES, 0, gridVAO->numberOfVertices);

        model = glm::rotate<float>(glm::mat4(1), glm::pi<float>() / 2, glm::vec3(0, 1, 0));
        glUniformMatrix4fv(6, 1, false, glm::value_ptr(model));
        glUniform1i(100, 1);
        glUniform4fv(101, 1, glm::value_ptr(glm::vec4(0.5, 0.2, 0.2, 1)));
        glUniform1i(102, 0);
        glUniform1i(104, 0);
        glDrawArrays(GL_LINES, 0, gridVAO->numberOfVertices);

}


bool f_initFreeType(FT_Library* ftlib) {
    FT_Error error = FT_Init_FreeType(ftlib);
    if (error) {
        return false;
    }
    return true;
}

bool f_createFontFace(FT_Library* ftlib, FT_Face* targetObject, const std::string& fontFileName) {
    FT_Error error = FT_New_Face(*ftlib, fontFileName.c_str(), 0, targetObject);
    return (error == 0);

}

bool f_setCharSize(FT_Face* face, int size) {
    //return FT_Set_Pixel_Sizes(*face, 0, 64) == 0;
    return (FT_Set_Char_Size(*face, size*64, 0, 96, 96) == 0);
}

bool f_renderTextToBuffer(TextObject* textObject, const std::string& text, uint8_t* pixelBuffer) {
    FT_Face face = textObject->_face;
    FT_GlyphSlot slot = face->glyph;
    int penX, penY, n;
    penX = 2;
    FT_Size size = face->size;

   for (n = 0; n < text.length(); n++) {

        FT_UInt glyph_index = FT_Get_Char_Index(face, text[n]);
        FT_Error error = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);
        if (error) {
            return false;
        }
        if (FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL)) {
            return false;
        }

        // actually draw the bitmap into the texture
        FT_Bitmap bitmap = slot->bitmap;
        int bearingX = face->glyph->bitmap_left;
        int bearingY = face->glyph->bitmap_top;


        for (int y = 0; y < face->glyph->bitmap.rows; y++) {
            for (int x = 0; x < face->glyph->bitmap.width; x++) {
                int i = x + y * face->glyph->bitmap.width;
                int ip = penX + bearingX + x + ((size->metrics.y_ppem - bearingY +y) * textObject->w);
                uint8_t val = face->glyph->bitmap.buffer[i];
              
                pixelBuffer[(4 * ip)] = val;
                pixelBuffer[(4 * ip) + 1] = val;
                pixelBuffer[(4 * ip) + 2] = val;
                pixelBuffer[(4 * ip) + 3] = val;
              
            }
        }
        penX += (slot->advance.x / 64);
    }

    return true;
    
}

FVAO* f_importModelFromFile(const std::string& fileName)
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(fileName,
        aiProcess_Triangulate
        //	| aiProcess_FlipUVs
    );

    if (scene == nullptr) {
        printf("error in import of file %s\n", fileName.c_str());
        exit(1);
    }

    std::vector<float> positions;
    std::vector<float> uvs;
    std::vector<float> normals;
    std::vector<uint32_t> indices;

    auto* mesh = scene->mMeshes[0];

    for (int x = 0; x < mesh->mNumVertices; x++)
    {
        aiVector3D pos = mesh->mVertices[x];
        positions.push_back(pos.x);
        positions.push_back(pos.y);
        positions.push_back(pos.z);

        if (mesh->HasNormals())
        {
            aiVector3D normal = mesh->mNormals[x];
            normals.push_back(normal.x);
            normals.push_back(normal.y);
            normals.push_back(normal.z);
        }

        if (mesh->HasTextureCoords(0))
        {
            aiVector3D uv = mesh->mTextureCoords[0][x];
            uvs.push_back(uv.x);
            uvs.push_back(uv.y);
        }
    }

    for (int f = 0; f < mesh->mNumFaces; f++)
    {
        aiFace face = mesh->mFaces[f];
        indices.push_back(face.mIndices[0]);
        indices.push_back(face.mIndices[1]);
        indices.push_back(face.mIndices[2]);
    }

    FVAO* fvao = new FVAO();
    f_createVAO(fvao);
    fvao->numberOfVertices = mesh->mNumVertices;

    FVBuffer vbPosition;
    f_createVertexBuffer(positions, &vbPosition);
    f_setVertexAttribute(0, GL_FLOAT, 3);

    
    



    FVBuffer vbNormals;
    f_createVertexBuffer(normals, &vbNormals);
    f_setVertexAttribute(2, GL_FLOAT, 3);

    FVBuffer vbUVs;
    f_createVertexBuffer(uvs, &vbUVs);
    f_setVertexAttribute(1, GL_FLOAT, 2);

    FIBuffer ib;
    f_createIndexBuffer(indices, &ib);
 
    return fvao;

}

TextObject::TextObject(FT_Face face, int numChars) {
    _face = face;
    // We calculate the expected size in pixels based on the estimated number of characters.
    w = face->size->metrics.x_ppem * numChars;
    h = face->size->metrics.y_ppem * 1.3;
    this->pixelBuffer = (uint8_t*)malloc(w * h * 4);
    memset(pixelBuffer, 255, w * h * 4);
    f_createTextureRGBA(w, h , pixelBuffer, &texture);
    glGenBuffers(2, pbos);
    index = 0;
    
}

void TextObject::setText(const std::string& text) {
    /*
    index = (index + 1) % 2;
    int nextIndex = (index + 1) % 2;
    GLenum err = glGetError();
    glBindTexture(GL_TEXTURE_2D, texture.texId);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbos[index]);

    err = glGetError();
    //glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, 0); 
    err = glGetError();

    memset(pixelBuffer, 0, w * h * 4);
    f_renderTextToBuffer(this, text, pixelBuffer);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbos[nextIndex]);
    err = glGetError();
    glBufferData(GL_PIXEL_UNPACK_BUFFER, w * h * 4, 0, GL_STREAM_DRAW);
    err = glGetError();
    GLubyte* ptr = (GLubyte*)glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);
    err = glGetError();
    if (ptr) {
        memcpy_s(ptr, w * h * 4, pixelBuffer, w * h * 4);
        glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
    }
    
    err = glGetError();
    
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pixelBuffer);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    err = glGetError();
    */

    glBindTexture(GL_TEXTURE_2D, texture.texId);
    memset(pixelBuffer, 0, w * h * 4);
    f_renderTextToBuffer(this, text, pixelBuffer);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pixelBuffer);
   
    glBindTexture(GL_TEXTURE_2D, 0);
    

}

FTimer::FTimer() {
    freq = SDL_GetPerformanceFrequency();
    lastDurationInSeconds = 0;
    startTick = 0;
}

void FTimer::stop() {
    lastDurationInSeconds = (float)(SDL_GetPerformanceCounter() - startTick) / (float)freq;
}

float FTimer::lastDurationInMillis() {
    return lastDurationInSeconds * 1000;
}

float FTimer::lastDurationInMicros() {
    return lastDurationInSeconds * 1000 * 1000;
}






