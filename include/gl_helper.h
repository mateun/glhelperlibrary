#pragma once
#include <GL/glew.h>
#include <vector>
#include <string>
#include "glm/glm.hpp"
#include <ft2build.h>
#include FT_FREETYPE_H
#include <SDL.h>


struct FVAO {
    GLuint vaoId;
    uint32_t numberOfVertices;
};

struct FShader {
    GLuint _shaderId;
};

struct FVBuffer {
    GLuint _id;
};

struct FIBuffer {
    GLuint _id;
};

struct FGLResult {
    bool ok;
    std::string errorMessage;
};

struct FTexture {
    GLuint texId;
    int w;
    int h;
};

struct Camera {
    glm::vec3 pos;
    glm::vec3 fwd;
    glm::mat4 viewMatrix;
    glm::mat4 projMatrix;

};

/**
*   This class represents a piece of (one line) text
*   which can be rendered as a texture.
*   
*/
struct TextObject {
    TextObject(FT_Face face, int numberOfCharacters);
    void setText(const std::string& text);
    
    FT_Face _face;
    FTexture texture;
    uint8_t* pixelBuffer;
    int w;
    int h;
    GLuint pbos[2];
    int index = 1;

};

struct FTimer {

    FTimer();

    void start() {
        startTick = SDL_GetPerformanceCounter();
    }
    void stop();
    float lastDurationInMillis();
    float lastDurationInMicros();
    

    uint64_t freq;
    uint64_t startTick;

    float lastDurationInSeconds;

};

FGLResult f_createShader(const std::string& vertexShaderSource, const std::string& fragmentShaderSource, FShader* target);
FGLResult f_createVertexBuffer(std::vector<float> data, FVBuffer* vb);
FGLResult f_createIndexBuffer(std::vector<uint32_t> data, FIBuffer* ib);
FGLResult f_createVAO(FVAO* target);
FGLResult f_unbindVAO();
FGLResult f_setVertexAttribute(GLuint slot, GLenum type, int nrOfComponents);
FGLResult f_setMatrixUniform(GLuint slot,  glm::mat4 mat);
FGLResult f_createTextureRGBA(int w, int h, void* pixelData, FTexture* target);
FGLResult f_createFrameBuffer(int w, int h, GLuint* colorTexture, GLuint* depthTexture, GLuint* fbo);
FVAO* createDebugGridVAO(int numberOfLines);
void drawDebugGrid(FVAO* gridVAO, const FShader& shader, const Camera& camera);
std::string readFile(const std::string& file);
FVAO* createRectVAO();
bool f_initFreeType(FT_Library* ftlib);
bool f_createFontFace(FT_Library* ftlib, FT_Face* targetObject, const std::string& fontFileName);
bool f_setCharSize(FT_Face* face, int size);
bool f_renderTextToBuffer(TextObject* textObject, const std::string& text, uint8_t* pixelBuffer);
FVAO* f_importModelFromFile(const std::string& fileName);
